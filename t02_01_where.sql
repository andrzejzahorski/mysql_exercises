use classicmodels;

# from -> where -> select -> order by
select lastName,
       firstName,
       jobTitle
from employees
where jobTitle = 'Sales Rep';

# where + and
select lastName,
       firstName,
       jobTitle,
       officeCode
from employees
where jobTitle = 'Sales Rep' and officeCode = 1;

# where + or
select lastName,
       firstName,
       employeeNumber,
       jobTitle,
       officeCode
from employees
where employeeNumber > 1200 or jobTitle = 'President';

# where + range (between)
select lastName,
       firstName,
       officeCode
from employees
where officeCode between 1 and 3;

# alfanum wildcards: _ - 1 char, % - 0 or more chars
select lastName,
       firstName
from employees
where lastName like '%son';

select lastName,
       firstName
from employees
where lastName like '_e%';

# parameters IN list
select lastName,
       firstName,
       officeCode
from employees
where officeCode in (1, 2, 3);

# looking for null values
select contactLastName,
       contactFirstName,
       addressLine2
from customers
where addressLine2 is null;


select contactLastName,
       contactFirstName,
       addressLine2
from customers
where addressLine2 is not null;

# Operators
/*
    =	Equal to. You can use it with almost any data types.
    <> or !=	Not equal to
    <	Less than. You typically use it with numeric and date/time data types.
    >	Greater than.
    <=	Less than or equal to
    >=	Greater than or equal to
*/

select orderNumber,
       productCode,
       priceEach * quantityOrdered as product_total
from orderdetails
where priceEach * quantityOrdered < 1000


