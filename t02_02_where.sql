# WHERE clause:
/*
select
    columns
from
    some_table
where
    search_specification


OUTPUT: True condition only

Order:
from -> where -> select -> order by
*/

use classicmodels;

select
    contactLastName,
    contactFirstName,
    state
from
    customers
where
    state = 'CA';

# Using  logical operators: NOT, AND, OR

/*
        AND

        True        False     NULL
_____________________________________

True    True        False     NULL

False   False       False     False

Null    Null        False     NULL

*/

select 1 = 0 and 1 / 0;

# WHERE + AND
select
    customerName,
    country,
    state
from
    customers
where
    country = 'USA' and state = 'NY';
/*
1: selecting customers table
2: applying where (USA and NY)
3: outputting selected rows
*/

select
    orderLineNumber,
    priceEach,
    quantityOrdered
from
    orderdetails
where
    orderLineNumber = 1
    and
    priceEach > 50
    and
    quantityOrdered > 30;


# === OR condition ===

/* OR
        True    |       False       |       Null
   
True    True            True                True
False   True            False               Null
Null    True            Null                Null
*/

# Short circuit evaluation:
select 1 = 1 or 1 / 0;

# AND is evaluated before OR:

select true or false and false;
#1: false and false: false
#2: true or false: true

# Changing order evaluation:
select (true or false) and false;

select
    customerName,
    country
from
    customers
where
    country = 'USA' or country = 'France'



SELECT
    customername,
    country,
    creditLimit
FROM
    customers
WHERE(country = 'USA'
        OR country = 'France')
      AND creditlimit > 100000;

select
    customerNumber
    customerName,
    city
from
    customers
where
    city not in ('USA', 'France');


# == Between ==

# element [not] between begin_val and end_val;
select 
    orderNumber,
    priceEach
from 
   orderdetails 
where
    priceEach BETWEEN 90 and 100.30;
# priceEach >= 90 and priceEach <= 100

select
    orderNumber,
    priceEach
from
   orderdetails
where
    priceEach not BETWEEN 90 and 100.30;

# priceEach < 90 and priceEach > 100
