use classicmodels;

# SELECT lista
# FROM tabela;

# FROM -> SELECT

select lastName
from employees;


select firstName,
       lastName,
       email
from employees;

select *
from employees;


# how to sort queries
/*
select
    what
from
    table_name
order by
    columnA [asc | desc]
    columnB [asc | desc];

from -> select -> order by
*/

# take last and first name and sort it by last name (ascending by default)
select lastName,
       firstName
from employees
order by lastName;

# take lname and Fname from table employees and sort it by lastname - descening
select lastName,
       firstName
from employees
order by lastName desc;

# print office codes of employyes sorted by officecode (descening)
select lastName, firstName, officeCode, jobTitle
from employees
order by officeCode desc;

# print emp info sorted by lastname (descending) and firstname (ascending)
select lastName,
       firstName
from employees
order by lastName desc,
         firstName asc;

# print order info sorted by total value of products (descending)
select orderNumber,
       quantityOrdered * priceEach
from orderdetails
order by quantityOrdered * priceEach desc;
# how to give alias to query
select orderNumber,
       quantityOrdered * priceEach as item_total
from orderdetails
order by item_total desc;

# how to sort data by custom lists
select orderNumber,
       status
from orders
order by field(status,
    'In Process',
    'On Hold',
    'Shipped',
    'Cancelled',
    'Disputed',
    'Resolved');
