# Distinct - removes duplicating rows of query, (does not sort output)
select distinct lastName
from employees;


select
    distinct state, city
from
    customers
where
    state is not null
order by
    state,
    city;

# distinct vs group by
# f you use the GROUP BY clause in the SELECT
# statement without using aggregate functions,
# the GROUP BY gives the same output like DISTINCT

select
    state
from
    customers
group by
    state;


select
    distinct state
from
    customers;


select
    count(distinct state) as count_state
from
    customers
where country = 'USA';

# LIMIT - limit search to specific count

select
    distinct state
from
    customers
where
    state is not null
limit 4;


select *
from
    customers
limit 5;


